set formatoptions=croql
set cindent
set comments=sr:/*,mb:*,ex:*/,://

set omnifunc=ccomplete#Complete
" configure tags - add additional tags here
set tags+=~/.vim/tags/c_lib
set tags+=~/.vim/tags/sys_lib

set tabstop=8
set noexpandtab
set softtabstop=8
set shiftwidth=8
