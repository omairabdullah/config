set formatoptions=croql

set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4	"number of spaces a tab counts for
set smarttab

set smartindent
