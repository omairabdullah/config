set formatoptions=croql
set cindent
set comments=sr:/*,mb:*,ex:*/,://

set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4	"number of spaces a tab counts for
set smarttab
