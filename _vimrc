" vim:fdm=marker
" vim:foldlevel=0
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File type options {{{

set nocompatible
syntax on

autocmd FileType *		set formatoptions=tcql comments&
autocmd BufRead,BufNewFile *.nfo set fileencoding=cp437 guifont=Terminal
"autocmd FileType lisp		set ai et sw=2 lisp showmatch
"autocmd FileType scheme	set ai et sw=2 lisp showmatch
"autocmd FileType cobol		set et sw=4 sts=4 sta tw=72
"autocmd FileType ada		set sw=3 si sts=3 sta tw=76
"autocmd FileType pascal	set sw=4 si sts=4
"autocmd FileType sml		set et si sw=4 sts=4 sta
"autocmd FileType asm		set et si sw=4 sts=4 sta syntax=mips
"autocmd BufRead,BufNewFile *.pro set filetype=prolog ai
"autocmd BufRead,BufNewFile *.asm set filetype=nasm ai sw=4

filetype plugin indent on

" Fall back to SyntaxComplete if no other autocomplete method available
if &omnifunc == ""
	setlocal omnifunc=syntaxcomplete#Complete
endif
"Option to save/restore 'views'
"au BufWinLeave * mkview
"au BufWinEnter * silent loadview

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Other options {{{

set tags=tags;$HOME
set grepprg=egrep\ -nIh\ --exclude=tags\ --exclude=cscope.out\ --exclude-dir=.git
set mouse=a

" call pathogen entry point
call pathogen#infect()
call pathogen#helptags()

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Visual & themes {{{

if has('gui_running')
  set guioptions-=T  " no toolbar
  set guioptions=Acgtm
"  set guifont=Droid\ Sans\ Mono,8
endif

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set color scheme {{{

"let g:solarized_termtrans=1
"let g:solarized_termcolors=256
let g:solarized_contrast="high"
let g:solarized_visibility="normal"

"set t_Co=16
colorscheme solarized
set background=light

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tabs, shifts and spaces {{{

" doesn't override file specific values
set formatoptions=tcrqn
set tabstop=8
"set softtabstop=8
"set shiftwidth=8
"set noexpandtab
set nowrap
"set smarttab

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Searching and highlighting {{{

let loaded_matchparen = 1
set showmatch
set mat=2
set hlsearch
set incsearch
set ignorecase
set smartcase

set backspace=2
set whichwrap+=<,>,h,l

" Highlight extra whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
"match ExtraWhitespace /\s\+\%#\@<!$/

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Status & display {{{

"set statusline=%F%m%r%h%w\ [%{&ff}]\ [%Y]\ [line\ %l\|col\ %v\|%p%%]\ [%L\ lines]
"set statusline=%F%m%r%h%w\ [%{&ff}]\ [%Y]\ [\%03.3b\ \0x%02.2B]\ [%4l\ line,%4v\ col\ %p%%]\ [%L\ totlines]
set laststatus=2

" Airline options
let g:airline#extensions#whitespace#show_message = 0
let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.paste = 'ρ'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#left_sep = '▶'
"let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = '〉'
"let g:airline_powerline_fonts = 1

set listchars=extends:>,precedes:<,eol:$,tab:▷⋅,trail:⋅,nbsp:⋅ " what to show when :set list
"set scrolloff=10 " Keep 10 lines (top/bottom) around cursor for scope
set novisualbell " don't blink
set noerrorbells " no noises
set noshowmode
set ruler " line, column and file position
set number "line numbers
set linespace=0  "line spacing
set textwidth=80
" highlight just the 81st column of wide lines
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 100)

set wildmenu
" Ignore compiled files
set wildignore=*.o,*~,*.pyc,*.ko

" Don't redraw while executing macros (good performance config)
set lazyredraw

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Folding {{{

" Enable folding, but by default make it act like folding is off, because folding is annoying in anything but a few rare cases
"noremap <space> za
nnoremap <silent> <Space> @=(foldlevel('.')?'za':'l')<CR>
vnoremap <Space> zf
set foldenable " Turn on folding
set foldcolumn=1 " Column on left to display folding '+'
set foldmethod=syntax " Make folding syntax sensitive
set foldlevel=100 " Don't autofold anything (but I can still fold manually)
"set foldopen-=search " don't open folds when you search into them
"set foldopen-=undo " don't open folds when you undo stuff
function! MyFoldText()
     "get first non-blank line
     let fs = v:foldstart
     while getline(fs) =~ '^\s*$' | let fs = nextnonblank(fs + 1)
     endwhile
     if fs > v:foldend
         let line = getline(v:foldstart)
     else
         let line = substitute(getline(fs), '\t', repeat(' ', &tabstop), 'g')
     endif

     let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
     let foldSize = 1 + v:foldend - v:foldstart
     let foldSizeStr = " " . foldSize . " lines "
     let foldLevelStr = repeat("+--", v:foldlevel)
     let lineCount = line("$")
     let foldPercentage = printf("[%.1f", (foldSize*1.0)/lineCount*100) . "%] "
     let expansionString = repeat(" ", w - strwidth(foldSizeStr.line.foldLevelStr.foldPercentage))
     return line . expansionString . foldSizeStr . foldPercentage . foldLevelStr
endfunction
au BufWinEnter,BufRead,BufNewFile * set foldtext=MyFoldText()

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" viminfo {{{

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo
set viminfo+=!

" To jump to saved cursor position in file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal!  g`\"" | endif

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spellcheck {{{

"setlocal spell spelllang=en_us
function! ToggleSpell()
    if !exists("b:spell")
        setlocal spell spelllang=en_us
        let b:spell = 1
    else
        setlocal nospell
        unlet b:spell
    endif
endfunction
set spellfile=~/.vim/dict.add
nmap <F4> :call ToggleSpell()<CR>
imap <F4> <Esc>:call ToggleSpell()<CR>a

" LanguageTool - grammar check
let g:languagetool_jar=$HOME.'/.vim/bundle/languagetool/lt21/LanguageTool.jar'

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File Encodings; ga, g8 commands {{{

" termencoding - how keyboard encodes what is typed
if &termencoding == ""
    let &termencoding = &encoding
endif

" The encoding for internal representation
set encoding=utf-8

" The encoding for all new files
"setglobal fileencoding=utf-8 bomb

" Heuristic to guess fileencoding
set fileencodings=ucs-bom,utf-8,latin1

" default file format
set fileformats=unix,dos,mac

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Other keyboard mappings {{{

" Go to vim command line
nnoremap ; :
" Save using Ctrl+s
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Reformat a para to respect textwidth
nmap <F2> gwap
imap <F2> <Esc>gwapa

" toggle background  to light/dark (solarized)
call togglebg#map("<F5>")

" Remove the Windows ^M - when the encodings gets messed up
"noremap <F6> mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Quickly open a buffer for scribbling
map <F7> :tabe ~/note.txt<cr>

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin options {{{

" For tagbar plugin
nnoremap <silent> <F8> :TagbarToggle<CR>
"let g:tagbar_autofocus = 1

" For encrypted files (:X)
set cryptmethod=blowfish

" For Mark plugin
let g:mwDefaultHighlightingPalette = [
	\   { 'ctermbg':'DarkCyan',   'ctermfg':'Black', 'guibg':'#D2A1FF', 'guifg':'#420080' },
	\   { 'ctermbg':'Magenta',    'ctermfg':'Black', 'guibg':'#FFA1C6', 'guifg':'#80005D' },
	\   { 'ctermbg':'Blue',       'ctermfg':'Black', 'guibg':'#A1B7FF', 'guifg':'#001E80' },
	\   { 'ctermbg':'DarkBlue',   'ctermfg':'Black', 'guibg':'#A1DBFF', 'guifg':'#004E80' },
	\   { 'ctermbg':'DarkRed',    'ctermfg':'Black', 'guibg':'#F5A1FF', 'guifg':'#720080' },
	\   { 'ctermbg':'DarkGreen',  'ctermfg':'Black', 'guibg':'#D0FFA1', 'guifg':'#3F8000' },
	\   { 'ctermbg':'Black',      'ctermfg':'White', 'guibg':'#53534C', 'guifg':'#DDDDDD' },
	\   { 'ctermbg':'DarkMagenta','ctermfg':'Black', 'guibg':'#A29CCF', 'guifg':'#120080' },
	\]

" Conque-term
let g:ConqueTerm_CloseOnEnd = 1
let g:ConqueTerm_CWInsert = 1

" Unite plugin for fuzzy searching of files
nnoremap <C-p> :Unite -start-insert file_rec/async<cr>
nnoremap <space>/ :Unite grep:.<cr>
call unite#custom_default_action('file,jump_list', 'tabopen')

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" HexMode {{{

" ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function ToggleHex()
    " hex mode should be considered a read-only operation
    " save values for modified and read-only for restoration later,
    " and clear the read-only flag for now
    let l:modified=&mod
    let l:oldreadonly=&readonly
    let &readonly=0
    let l:oldmodifiable=&modifiable
    let &modifiable=1
    if !exists("b:editHex") || !b:editHex
        " save old options
        let b:oldft=&ft
        let b:oldbin=&bin
        " set new options
        setlocal binary " make sure it overrides any textwidth, etc.
        let &ft="xxd"
        " set status
        let b:editHex=1
        " switch to hex editor
        %!xxd
    else
        " restore old options
        let &ft=b:oldft
        if !b:oldbin
            setlocal nobinary
        endif
        " set status
        let b:editHex=0
        " return to normal editing
        %!xxd -r
    endif
    " restore values for modified and read only state
    let &mod=l:modified
    let &readonly=l:oldreadonly
    let &modifiable=l:oldmodifiable
endfunction

" autocmds to automatically enter hex mode and handle file writes properly
if has("autocmd")
  " vim -b : edit binary using xxd-format!
  augroup Binary
    au!

    " set binary option for all binary files before reading them
    au BufReadPre *.bin,*.hex setlocal binary

    " if on a fresh read the buffer variable is already set, it's wrong
    au BufReadPost *
          \ if exists('b:editHex') && b:editHex |
          \   let b:editHex = 0 |
          \ endif

    " convert to hex on startup for binary files automatically
    au BufReadPost *
          \ if &binary | Hexmode | endif

    " When the text is freed, the next time the buffer is made active it will
    " re-read the text and thus not match the correct mode, we will need to
    " convert it again if the buffer is again loaded.
    au BufUnload *
          \ if getbufvar(expand("<afile>"), 'editHex') == 1 |
          \   call setbufvar(expand("<afile>"), 'editHex', 0) |
          \ endif

    " before writing a file when editing in hex mode, convert back to non-hex
    au BufWritePre *
          \ if exists("b:editHex") && b:editHex && &binary |
          \  let oldro=&ro | let &ro=0 |
          \  let oldma=&ma | let &ma=1 |
          \  silent exe "%!xxd -r" |
          \  let &ma=oldma | let &ro=oldro |
          \  unlet oldma | unlet oldro |
          \ endif

    " after writing a binary file, if we're in hex mode, restore hex mode
    au BufWritePost *
          \ if exists("b:editHex") && b:editHex && &binary |
          \  let oldro=&ro | let &ro=0 |
          \  let oldma=&ma | let &ma=1 |
          \  silent exe "%!xxd" |
          \  exe "set nomod" |
          \  let &ma=oldma | let &ro=oldro |
          \  unlet oldma | unlet oldro |
          \ endif
  augroup END
endif

""" Convert number under cursor to its hex/dec value
nnoremap gn :call DecAndHex(expand("<cWORD>"))<CR>

function! DecAndHex(number)
    let ns = '[.,;:''"<>()^_lL]'      " number separators
    if a:number =~? '^' . ns. '*[-+]\?\d\+' . ns . '*$'
        let dec = substitute(a:number, '[^0-9+-]*\([+-]\?\d\+\).*','\1','')
        echo dec . printf('  ->  0x%X, -(0x%X)', dec, -dec)
    elseif a:number =~? '^' . ns.  '*\%\(h''\|0x\|#\)\?\(\x\+\)' . ns .  '*$'
        let hex = substitute(a:number, '.\{-}\%\(h''\|0x\|#\)\?\(\x\+\).*','\1','')
        echon '0x' . hex . printf(' ->  %d', eval('0x'.hex))
        if strpart(hex, 0,1) =~?  '[89a-f]' && strlen(hex) =~?  '2\|4\|6'
            " for 8/16/24 bits numbers print the equivalent negative number
            echon ' ('.  float2nr(eval('0x'.  hex) - pow(2,4*strlen(hex))) .  ')'
        endif
        echo
    else
        echo
        "NaN"
    endif
endfunction

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
