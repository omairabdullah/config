#
# Example of a user's .screenrc file
#

# This is how one can set a reattach password:
# password ODSJQf.4IJN7E    # "1234"

# no annoying audible bell, please
vbell off

# detach on hangup
autodetach on

# don't display the copyright page
startup_message off

# emulate .logout message
pow_detach_msg "Screen session of \$LOGNAME \$:cr:\$:nl:ended."

msgwait 3

# advertise hardstatus support to $TERMCAP
termcapinfo  * '' 'hs:ts=\E_:fs=\E\\:ds=\E_\E\\'
term screen-256color

# make the shell in every window a login shell
#shell -$SHELL

# autoaka testing
# shellaka '> |tcsh'
# shellaka '$ |sh'

# set every new windows hardstatus line to somenthing descriptive
#defhstatus "screen: ^En (^Et)"

defscrollback 4000

# don't kill window after the process died
# zombie "^["

# enable support for the "alternate screen" capability in all windows
altscreen on

################
#
# xterm tweaks
#

#xterm understands both im/ic and doesn't have a status line.
#Note: Do not specify im and ic in the real termcap/info file as
#some programs (e.g. vi) will not work anymore.
termcap  xterm hs@:cs=\E[%i%d;%dr:im=\E[4h:ei=\E[4l
terminfo xterm hs@:cs=\E[%i%p1%d;%p2%dr:im=\E[4h:ei=\E[4l

#80/132 column switching must be enabled for ^AW to work
#change init sequence to not switch width
termcapinfo  xterm Z0=\E[?3h:Z1=\E[?3l:is=\E[r\E[m\E[2J\E[H\E[?7h\E[?1;4;6l

# Make the output buffer large for (fast) xterms.
termcapinfo xterm* OL=5000
#termcapinfo xterm* OL=100

# tell screen that xterm can switch to dark background and has function
# keys.
#termcapinfo xterm 'VR=\E[?5h:VN=\E[?5l'
#termcapinfo xterm 'k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~'
#termcapinfo xterm 'kh=\EOH:kI=\E[2~:kD=\E[3~:kH=\EOF:kP=\E[5~:kN=\E[6~'

# special xterm hardstatus: use the window title.
#termcapinfo xterm 'hs:ts=\E]2;:fs=\007:ds=\E]2;screen\007'

#terminfo xterm 'vb=\E[?5h$<200/>\E[?5l'
#termcapinfo xterm 'vi=\E[?25l:ve=\E[34h\E[?25h:vs=\E[34l'

# emulate part of the 'K' charset
termcapinfo   xterm 'XC=K%,%\E(B,[\304,\\\\\326,]\334,{\344,|\366,}\374,~\337'

# xterm-52 tweaks:
# - uses background color for delete operations
#termcapinfo xterm* be
#termcapinfo xterm* ti@:te@

# Extend the vt100 desciption by some sequences.
#termcap  vt100* ms:AL=\E[%dL:DL=\E[%dM:UP=\E[%dA:DO=\E[%dB:LE=\E[%dD:RI=\E[%dC
#terminfo vt100* ms:AL=\E[%p1%dL:DL=\E[%p1%dM:UP=\E[%p1%dA:DO=\E[%p1%dB:LE=\E[%p1%dD:RI=\E[%p1%dC
#termcapinfo linux C8
# old rxvt versions also need this
# termcapinfo rxvt C8


################
#
# keybindings
#

#remove some stupid / dangerous key bindings
bind k
bind ^k
bind .
bind ^\
bind \\
bind ^h
bind h
#make them better
bind 'K' kill
bind 'I' login on
bind 'O' login off
bind '}' history

#escape '^@^@'
#bind 'y' other

# To set the title of screen to the current running program
shelltitle "$ |bash"
#shelltitle ''
#termcapinfo xterm*|rxvt*|kterm*|Eterm* 'hs:ts=\E]0;:fs=\007:ds=\E]0;\007'
defhstatus "screen ^E (^Et) | $USER@^EH"
#hardstatus off

hardstatus alwayslastline
hardstatus string '%{= wk}[%{= wY}%?%-Lw%?%{+b WK} %n %t%?(%u)%? %{-b wK} %{wY}%?%+Lw%?%?%= %{k}][%{wC}%H%{wM} %D %d/%M %{wb}%c %{k}]'
#hardstatus string '%{= kG}[%{= kw}%?%-Lw%?%{r}(%{+b W}%n %t%?(%u)%?%{-b r})%{w}%?%+Lw%?%?%= %{g}][%{yC}%H%{kB} %D %d/%M %{W}%c %{g}]'
#hardstatus string '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %d/%m %{W}%c %{g}]'
#hardstatus string '%{= mK}%-Lw%{= KW}%50>%n%f* %t%{= mK}%+Lw%< %{= kG}%-=%D %d %M %Y %c:%s%{-}'

#########
##byobu##
#########
#source $HOME/.byobu/keybindings
#hardstatus string '%12`%?%-Lw%50L>%?%{=r}%n*%f %t%?(%u)%?%{-}%12`%?%+Lw%?%11` %=%12`%110`%109`%122`%111`%10`%< %99`%{-}%{=r}%12` %100`%112`%= %130`%102`%101`%129`%131`%127`%114`%115`%108`%134`%128`%125`%126`%113`%119`%133`%117`%116`%106`%104`%103`%105`%107`%123`%132`%120`%121`'

# Monitor windows
defmonitor on
activity ""

#########

# Yet another hack:
# Prepend/append register [/] to the paste if ^a^] is pressed.
# This lets me have autoindent mode in vi.
register [ "\033:se noai\015a"
register ] "\033:se ai\015a"
bind ^] paste [.]

################
#
# default windows
#

# screen -t local 0
# screen -t mail 1 mutt
# screen -t 40 2 rlogin server

# caption always "%3n %t%? @%u%?%? [%h]%?%=%c"
# hardstatus alwaysignore
# hardstatus alwayslastline "%Lw"

# bind = resize =
# bind + resize +1
# bind - resize -1
# bind _ resize max
#
# defnonblock 1
# blankerprg rain -d 100
# idle 30 blanker
#screen

#stuff "export PS1='\[\033k\033\\\]\u@\h:\w\$ '\012"
#stuff "screen -ls\012\012\012"

#    Keybindings    ##

# bind <F7> to detach screen from this terminal
# bind <F8> to kill current session
# bind <F10> to create a new screen
# bind <F9> to rename an existing window
# bind <F11> to move to previous window
# bind <F12> to move to next window
#bindkey -k k7 detach
#bindkey -k k8 kill
# space in keyboard
#bindkey -k k; screen
#bindkey -k k9 title
#bindkey -k F1 prev
#bindkey -k F2 next
# F-keys seem to work well in both gnome-terminal and tty consoles
bindkey -k k2 prev	
bindkey -k k3 next	
bindkey -k k4 other	
bindkey -k k5 copy	
bindkey -k k9 detach
#bindkey -k k1 screen -t config 0 byobu-config		# F1  | Configuration (along with F9)
							#     | since F1 = Help in gnome-terminal
#bindkey -k k2 screen					# F2  | Create new window
#bindkey -k k3 prev					# F3  | Previous Window
#bindkey -k k4 next					# F4  | Next Window
#register r "^a:source $HOME/.byobu/profile^M"		#     | Goes with F5 definition
#bindkey -k k5 eval 'fit' 'process r'			# F5  | Reload profile
#bindkey -k k6 detach					# F6  | Detach from this session
##bindkey -k k7 copy					# F7  | Enter copy/scrollback mode
#register t "^aA^aa^k^h"					#     | Goes with the F8 definition
#bindkey -k k8 process t 				# F8  | Re-title a window
#bindkey -k k9 screen -t config 0 byobu-config		# F9  | Configuration
#							# F10 | 'toolbar' in gnome-terminal
##							# F11 | 'fullscreen' in gnome-terminal
bindkey -k F2 lockscreen				# F12 | Lock this terminal

#screen
#stuff "screen -ls\012\012\012"

